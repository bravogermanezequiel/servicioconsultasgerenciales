from os import environ

class Config:
    FLASK_ENV = environ.get("FLASK_ENV")
    
class ConfigTest(Config):
    TESTING = True
    DEBUG = True
