FROM python:3.9
WORKDIR /gerencial
COPY . /gerencial/
RUN pip install -r requirements.txt