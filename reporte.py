from excepciones import *
import datetime
from auxiliar import ConsultorOrdenes, ConsultorLibros, ConsultorSocios, ConsultorGeneros

class Reporte:
    def libroMasVendidoPorMesEnAnio(self, anio, mes):
        if mes < 1 or mes > 12:
            raise MesFueraRangoException
            
        ordenes = ConsultorOrdenes.getOrdenes(anio, mes)
        
        if not len(ordenes):
            raise OrdenNotFoundException
        
        librosCantidades = {}

        for orden in ordenes:
            for idLibro, cantidad in orden["libros"]:
                librosCantidades[idLibro] = librosCantidades.get(idLibro, 0) + cantidad
        
        maxIdLibro = max(librosCantidades, key=librosCantidades.get)

        libro = ConsultorLibros.obtenerLibro(maxIdLibro)

        return {
            "id": libro["id"],
            "titulo": libro["titulo"],
            "autorId": libro["autorId"],
            "editorialId": libro["editorialId"],
            "generoId": libro["generoId"]
        }

    def socioConMasEnvios(self):
        socioMaxEnvios = ConsultorOrdenes.obtenerSocioMayorEnvios()

        if not socioMaxEnvios or not socioMaxEnvios["cantidadEnvios"]:
            raise EnvioNotFoundException
    
        socio = ConsultorSocios.obtenerSocio(socioMaxEnvios["socioId"])

        return {
            "socioId": socioMaxEnvios["socioId"],
            "cantidadEnvios": socioMaxEnvios["cantidadEnvios"],
            "nombre": socio["nombre"],
            "apellido": socio["apellido"]
        }
    
    def generoMasVendidoPorMesEnAnio(self, anio, mes):
        if mes < 1 or mes > 12:
            raise MesFueraRangoException

        ordenes = ConsultorOrdenes.getOrdenes(anio, mes)
        
        if not len(ordenes):
            raise OrdenNotFoundException
        
        librosCantidades = {}
        for orden in ordenes:
            for idLibro, cantidad in orden["libros"]:
                librosCantidades[idLibro] = librosCantidades.get(idLibro, 0) + cantidad

        generosCantidades = {}
        for libroId, cantidadLibro in librosCantidades.items():
            libro = ConsultorLibros.obtenerLibro(libroId)

            if libro:
                generosCantidades[libro["generoId"]] = generosCantidades.get(libro["generoId"], 0) + cantidadLibro
        
        if not generosCantidades:
            raise GenerosNotFoundException

        maxIdGenero = max(generosCantidades, key=generosCantidades.get)

        genero = ConsultorGeneros.obtenerGenero(maxIdGenero)

        return {
            "generoId": maxIdGenero,
            "cantidadVentas": generosCantidades[maxIdGenero],
            "nombre": genero["nombre"]
        }