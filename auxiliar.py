from config_uri import URL_LIBRERIA, URL_ORDENES
from lib.RequestWrapper import RequestWrapper

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

class ConsultorLibros:
    @staticmethod
    def obtenerLibro(id):
        URL = f"{URL_LIBRERIA}/libro/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != 200:
            return None

        return r.json()

class ConsultorSocios:
    @staticmethod
    def obtenerSocio(id):
        URL = f"{URL_LIBRERIA}/socio/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != 200:
            return None
            
        return r.json()

class ConsultorGeneros:
    @staticmethod
    def obtenerGenero(id):
        URL = f"{URL_LIBRERIA}/genero/{id}"
        r = RequestWrapper.get(URL)
        
        if r.status_code != 200:
            return None
            
        return r.json()

class ConsultorOrdenes:
    @staticmethod
    def getOrdenes(anio, mes):
        URL = f"{URL_ORDENES}/ordenes/{anio}/{mes}"
        r = RequestWrapper.get(URL)

        if r.status_code != HTTP_OK_REQUEST:
            return None
        
        data = r.json()

        return data.get("ordenes")
    
    @staticmethod
    def obtenerSocioMayorEnvios():
        URL = f"{URL_ORDENES}/orden/socio/mayorEnvios"
        r = RequestWrapper.get(URL)

        if r.status_code != HTTP_OK_REQUEST:
            return None
        
        return r.json()