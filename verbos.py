from flask import Blueprint, jsonify, request
from reporte import Reporte
from excepciones import *
import logging

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

ruta = Blueprint("rutas", __name__, url_prefix="")

@ruta.route("/", methods=["GET"])
def server_info():
    return jsonify("API Reportes Gerenciales"), HTTP_OK_REQUEST

@ruta.route("/reporte/libro/<int:anio>/<int:mes>", methods=["GET"])
def libroMasVendidoPorMesEnAnio(anio, mes):
    try:
        res = Reporte().libroMasVendidoPorMesEnAnio(anio, mes)
        return jsonify(res), HTTP_OK_REQUEST
    except OrdenNotFoundException:
        return jsonify({"Resultado": "No existen ordenes para tal año y fecha"}), HTTP_NOT_FOUND
    except MesFueraRangoException:
        return jsonify({"Resultado": "El valor del mes debe estar en el rango de entre 1..12"}), HTTP_BAD_REQUEST

@ruta.route("/reporte/socio/mayorEnvios", methods=["GET"])
def socioConMasEnvios():
    try:
        res = Reporte().socioConMasEnvios()
        return jsonify(res), HTTP_OK_REQUEST
    except EnvioNotFoundException:
        return jsonify({"Resultado": "No existen envios"}), HTTP_NOT_FOUND

@ruta.route("/reporte/genero/<int:anio>/<int:mes>", methods=["GET"])
def generoMasVendidoPorMesEnAnio(anio, mes):
    try:
        res = Reporte().generoMasVendidoPorMesEnAnio(anio, mes)
        return jsonify(res), HTTP_OK_REQUEST
    except GenerosNotFoundException:
        return jsonify({"Resultado": "No fue posible mapear libros a ningun genero existente para tal año y fecha"}), HTTP_NOT_FOUND
    except OrdenNotFoundException:
        return jsonify({"Resultado": "No existen ordenes para tal año y fecha"}), HTTP_NOT_FOUND
    except MesFueraRangoException:
        return jsonify({"Resultado": "El valor del mes debe estar en el rango de entre 1..12"}), HTTP_BAD_REQUEST

@ruta.before_request
def beforeRequest():
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    body = request.data.replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Request. request.method={method} request.endpoint={endpoint} request.body='{body}'")

@ruta.after_request
def afterRequest(response):
    log = logging.getLogger('RECEIVEREQUEST_LOGGING')
    endpoint = request.path
    method = request.method
    data = response.get_data().replace(b'\n', b'').decode('utf-8')
    log.info(f"[Entrante] Response. response.method={method} response.endpoint={endpoint} response.data='{data}'")
    return response