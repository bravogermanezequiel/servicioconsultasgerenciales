# ¿Que tests se realizaron?

Se llevaron a cabo tests sobre todas operaciones referidas a verbos API de forma unitaria. Para todo se verifica la salida y codigo de respuesta esperados.

## Consultas
**Se establece un setUp de dos transportes con diferentes patentes y disponibilidades**

- Libro mas vendido por mes: Se mockeo las respuesta del metodo de obtener ordenes y obtener libro, se llama a la ruta "/reporte/libro/2021/01", luego se verifica que sea correcto el calculo del libro mas vendido y devuelva los datos correctos del libro.

- Libro mas vendido por mes sin libro para la fecha: Se mockeo las respuesta del metodo de obtener ordenes para que nos devuelva una lista vacia (No se genero orden para año y mes), se llama a la ruta "/reporte/libro/2021/01" ,se verifica que devuelva un codigo de erro 404 ya que no existe libro mas vendido de ese mes.

- Socio mayor envio: Se mockeo las respuesta del metodo de obtener socio mayor envio y obtener socio, se llama a la ruta "/reporte/socio/mayorEnvios", se verifica que devuelva los datos correcto y que se llame al metodo de obtener socio con el id correcto.

- Socio mayor envio sin envios:Se mockeo las respuesta del metodo de obtener socio mayor envio para que nos devuelva una None(No se genero envio para año y mes), se llama a la ruta "/reporte/socio/mayorEnvios", se verifica que devuelva un codigo de error 404 ya que no existe socio con mas envios.

- Genero mas vendido por mes: Se mockeo las respuesta del metodo de obtener ordenes, obtener libro(mock con respuesta distinta dependiendo id libro) y obtener genero. Se llama a la ruta "/reporte/genero/2021/01", se verifica que devuelva datos correctos, ademas que se llame al metodo de obtener genero con el id de genero correcto y que la cantidad de llamados a obtener libro sea correcto.

- Genero mas vendido por mes, con repeticion genero: Repetimos test anterior pero con libros que tiene el mismo genero, se hace mismas verificacion, solo cambio la respuesta del mock de obtener ordenes