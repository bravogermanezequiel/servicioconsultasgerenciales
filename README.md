# API Consultas Gerenciales

## Rutas

- Reporte
    1. Obtener libro mas vendido por mes y año: GET /reporte/libro/{int:anio}/{int:mes}
    2. Obtener socio con mayor envios realizados: GET /reporte/socio/mayorEnvios
    3. Obtener genero mas vendido por mes y año: GET /reporte/genero/{int:anio}/{int:mes}
