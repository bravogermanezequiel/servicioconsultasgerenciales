from flask import Flask, jsonify
from prometheus_flask_exporter import PrometheusMetrics

def initApp(config="Config"):
    from verbos import ruta

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(f"config.{config}")
    app.register_blueprint(ruta)
    metrics = PrometheusMetrics(app)
    
    return app