from app import initApp
from config_log import CONFIG_LOGGING
import logging
import logging.config

logging.config.dictConfig(CONFIG_LOGGING)
app = initApp()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3000)