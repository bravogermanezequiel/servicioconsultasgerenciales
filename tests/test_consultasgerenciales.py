from . import BaseTestClass
from auxiliar import ConsultorLibros, ConsultorOrdenes, ConsultorSocios, ConsultorGeneros
from mock import patch
from excepciones import *

def sideEffectFuncGetLibro(id):
    if id == 1:
        return {
            "id": 1,
            "titulo": "Caperucita Roja",
            "generoId": 1,
            "editorialId": 1,
            "autorId": 1,
            "volumen": 2
        }
    elif id == 2:
        return {
            "id": 2,
            "titulo": "Max Payne",
            "generoId": 2,
            "editorialId": 3,
            "autorId": 2,
            "volumen": 1
        }
    elif id == 3:
        return {
            "id": 3,
            "titulo": "Chulet",
            "generoId": 3,
            "editorialId": 1,
            "autorId": 1,
            "volumen": 1
        }
    elif id == 4:
        return {
            "id": 4,
            "titulo": "Los 7 enanitos",
            "generoId": 1,
            "editorialId": 2,
            "autorId": 3,
            "volumen": 2
        }

class TestConsultasGerenciales(BaseTestClass):
    @patch.object(ConsultorLibros, "obtenerLibro")
    @patch.object(ConsultorOrdenes, "getOrdenes")
    def testLibroMasVendidoOk(self, mockGetOrdenes, mockGetLibro):
        mockGetOrdenes.return_value = [
            {
                "enviado": False,
                "envioId": 1,
                "libros": [
                    [1, 2],
                    [3, 10],
                    [4, 5]
                ],
                "fecha": "2021-01-01",
                "procesado": True
            },
            {
                "enviado": False,
                "envioId": 2,
                "libros": [
                    [1, 10],
                    [3, 1]
                ],
                "fecha": "2021-01-05",
                "procesado": True
            }
        ]

        mockGetLibro.return_value = {
            "id": 1,
            "titulo": "Caperucita roja",
            "generoId": 2,
            "editorialId": 1,
            "autorId": 1,
            "volumen": 2
        }

        res = self.client.get("/reporte/libro/2021/01")

        args, kwargs = mockGetLibro.call_args
        assert args[0] == 1

        assert res.status_code == 200
        
        jsonData = res.json

        assert jsonData["id"] == 1
        assert jsonData["titulo"] == "Caperucita roja"
        assert jsonData["generoId"] == 2
        assert jsonData["editorialId"] == 1
        assert jsonData["autorId"] == 1
    
    @patch.object(ConsultorLibros, "obtenerLibro")
    @patch.object(ConsultorOrdenes, "getOrdenes")
    def testLibroMasVendidoNoHayLibrosParaFecha(self, mockGetOrdenes, mockGetLibro):
        mockGetOrdenes.return_value = []

        res = self.client.get("/reporte/libro/2021/01")

        assert mockGetLibro.call_count == 0
        assert res.status_code == 404
        assert res.json["Resultado"] == "No existen ordenes para tal año y fecha"
    
    @patch.object(ConsultorSocios, "obtenerSocio")
    @patch.object(ConsultorOrdenes, "obtenerSocioMayorEnvios")
    def testSocioMayorEnvios(self, mockGetSocioMayorEnvios, mockGetSocio):
        mockGetSocio.return_value = {
            "id": 1,
            "nombre": "Pepito",
            "apellido": "Balvuena",
            "edad": 20
        }

        mockGetSocioMayorEnvios.return_value = {
            "socioId": 1,
            "cantidadEnvios": 5
        }

        res = self.client.get("/reporte/socio/mayorEnvios")

        argsGetSocio, kwargs = mockGetSocio.call_args

        assert argsGetSocio[0] == 1
        assert res.status_code == 200
        
        jsonData = res.json

        assert jsonData["socioId"] == 1
        assert jsonData["cantidadEnvios"] == 5
        assert jsonData["nombre"] == "Pepito"
        assert jsonData["apellido"] == "Balvuena"
    
    @patch.object(ConsultorSocios, "obtenerSocio")
    @patch.object(ConsultorOrdenes, "obtenerSocioMayorEnvios")
    def testSocioMayorEnviosNoHayEnvios(self, mockGetSocioMayorEnvios, mockGetSocio):
        mockGetSocioMayorEnvios.return_value = None

        res = self.client.get("/reporte/socio/mayorEnvios")

        assert res.status_code == 404
        assert mockGetSocio.call_count == 0

        jsonData = res.json

        assert jsonData["Resultado"] == "No existen envios"
    
    @patch.object(ConsultorLibros, "obtenerLibro")
    @patch.object(ConsultorOrdenes, "getOrdenes")
    @patch.object(ConsultorGeneros, "obtenerGenero")
    def testGeneroMasVendidoOk(self, mockGetGenero, mockGetOrdenes, mockGetLibro):
        mockGetOrdenes.return_value = [
            {
                "enviado": False,
                "envioId": 1,
                "libros": [
                    [1, 2],
                    [2, 10],
                    [3, 5]
                ],
                "fecha": "2021-01-01",
                "procesado": True
            },
            {
                "enviado": False,
                "envioId": 2,
                "libros": [
                    [1, 10],
                    [2, 1]
                ],
                "fecha": "2021-01-05",
                "procesado": True
            }
        ]

        mockGetLibro.side_effect = sideEffectFuncGetLibro

        mockGetGenero.return_value = {
            "id": 1,
            "nombre": "Cuentos infantiles"
        }

        res = self.client.get("/reporte/genero/2021/01")

        args, kwargs = mockGetGenero.call_args
        assert args[0] == 1

        assert res.status_code == 200
        
        assert mockGetLibro.call_count == 3

        jsonData = res.json

        assert jsonData["generoId"] == 1
        assert jsonData["cantidadVentas"] == 12
        assert jsonData["nombre"] == "Cuentos infantiles"

    @patch.object(ConsultorLibros, "obtenerLibro")
    @patch.object(ConsultorOrdenes, "getOrdenes")
    @patch.object(ConsultorGeneros, "obtenerGenero")
    def testGeneroMasVendidoOkConRepeticionDeGeneros(self, mockGetGenero, mockGetOrdenes, mockGetLibro):
        mockGetOrdenes.return_value = [
            {
                "enviado": False,
                "envioId": 1,
                "libros": [
                    [1, 2],
                    [2, 10],
                    [3, 5]
                ],
                "fecha": "2021-01-01",
                "procesado": True
            },
            {
                "enviado": False,
                "envioId": 2,
                "libros": [
                    [1, 5],
                    [2, 1],
                    [4, 6]
                ],
                "fecha": "2021-01-05",
                "procesado": True
            }
        ]

        mockGetLibro.side_effect = sideEffectFuncGetLibro

        mockGetGenero.return_value = {
            "id": 1,
            "nombre": "Cuentos infantiles"
        }

        res = self.client.get("/reporte/genero/2021/01")

        args, kwargs = mockGetGenero.call_args
        assert args[0] == 1

        assert res.status_code == 200
        
        assert mockGetLibro.call_count == 4

        jsonData = res.json

        assert jsonData["generoId"] == 1
        assert jsonData["cantidadVentas"] == 13
        assert jsonData["nombre"] == "Cuentos infantiles"