import unittest
from app import initApp

class BaseTestClass(unittest.TestCase):
    def setUp(self):
        self.app = initApp("ConfigTest")
        self.client = self.app.test_client()